/*
 * sydbox/syd-addr.c
 * Syd's Address Hash Header Compiler
 *
 * Copyright (c) 2021 Ali Polatel <alip@exherbo.org>
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include "HELPME.h"
#include "syd-conf.h"
#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <getopt.h>
#include <syd/syd.h>
#include <sqlite3.h>
#include "macro.h"

#define streq(a,b) (strcmp((a),(b)) == 0)
#define streqcase(a,b) (strcasecmp((a),(b)) == 0)

#ifdef PACKAGE
# undef PACKAGE
#endif
#define PACKAGE "syd-addr"

static void about(void);
static void usage(FILE *outfp, int code)
	SYD_GCC_ATTR((noreturn));

static void die(const char *fmt, ...);
static void say(const char *fmt, ...);
static void vsay(FILE *fp, const char *fmt, va_list ap, char level);

SYD_GCC_ATTR((nonnull(1)))
static int db_init(const char *path, bool create, bool readonly);
static void db_close(void);
static int db_create(void);
static int db_check_version(void);

SYD_GCC_ATTR((nonnull(1)))
static int db_step(sqlite3_stmt *stmt);
static int db_set_sync(bool on);
static int db_vacuum(void);
static int db_start_transaction(void);
static int db_end_transaction(void);

SYD_GCC_ATTR((nonnull(1)))
static int db_set_authorizer(int (*xAuth)(void *, int, const char *, const char *,
					 const char *,const char *),
			    void *userdata);

SYD_GCC_ATTR((nonnull(1)))
static int db_id2header(const char *path_dst);

static sqlite3 *gdb = NULL;

enum {
	SQL_SET_VERSION,
	SQL_GET_VERSION,
	SQL_SET_ENCODING,

	SQL_BEGIN_TRANSACTION,
	SQL_END_TRANSACTION,
	SQL_ROLLBACK_TRANSACTION,

	SQL_PRAGMA_SYNC_ON,
	SQL_PRAGMA_SYNC_OFF,

	SQL_VACUUM,
};

enum {
	SQL_DB_CREATE_ADDR,
};

enum {
	SQL_DB_COUNT_ADDR,
	SQL_DB_SELECT_ADDR_ID,
};

#define DB_PATH "addr.db"
#define HR_PATH "syd-net-addr.h"
#define DB_MINIMUM_VERSION 1984
#define DB_VERSION	1984

/* Generic database schema independent statements */
static const char * const db_sql_maint[] = {
	[SQL_SET_VERSION] = "PRAGMA user_version = "syd_str(DB_VERSION)";",
	[SQL_GET_VERSION] = "PRAGMA user_version;",

	[SQL_SET_ENCODING] = "PRAGMA encoding = \"UTF-8\";",

	[SQL_BEGIN_TRANSACTION] = "BEGIN TRANSACTION;",
	[SQL_END_TRANSACTION] = "END TRANSACTION;",
	[SQL_ROLLBACK_TRANSACTION] = "ROLLBACK TRANSACTION;",

	[SQL_PRAGMA_SYNC_ON] = "PRAGMA synchronous=ON;",
	[SQL_PRAGMA_SYNC_OFF] = "PRAGMA synchronous=OFF;",

	[SQL_VACUUM] = "VACUUM;",
};
static sqlite3_stmt *db_stmt_maint[ELEMENTSOF(db_sql_maint)] = { NULL };

/* Statements for creating a new database */
static const char * const db_sql_create[] = {
	[SQL_DB_CREATE_ADDR] =
		"create table if not exists addr(\n"
			"\tid              INTEGER PRIMARY KEY,\n"
			"\tdns             TEXT NOT NULL DEFAULT NULL,\n"
			"\tsrc             TEXT NOT NULL DEFAULT NULL,\n"
			"\tip              TEXT NOT NULL DEFAULT NULL,\n"
			"\tf               INTEGER NOT NULL DEFAULT 0,\n"
			"\tmtime           DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,\n"
			"\tctime           DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL\n"
			");\n",
};
static sqlite3_stmt *db_stmt_create[ELEMENTSOF(db_sql_maint)] = { NULL };

static const char * const db_sql[] = {
	[SQL_DB_COUNT_ADDR] = "select count(id) from addr;",
	[SQL_DB_SELECT_ADDR_ID] = "select id from addr order by id;",
};
static sqlite3_stmt *db_stmt[ELEMENTSOF(db_sql)] = { NULL };

int main(int argc, char **argv)
{
	int opt;
	struct option long_options[] = {
		/* default options */
		{"help",	no_argument,		NULL,	'h'},
		{"version",	no_argument,		NULL,	'v'},
		{"db",		required_argument,	NULL,	'd'},
		{"output",	required_argument,	NULL,	'o'},
	};

	int options_index, r = 0;
	const char *db_path = DB_PATH;
	const char *out_path = HR_PATH;
	while ((opt = getopt_long(argc, argv, "hvd:o:", long_options,
				  &options_index)) != EOF) {
		switch (opt) {
		case 'h':
			usage(stdout, 0);
			return 0;
		case 'v':
			about();
			syd_about(stdout);
			return 0;
		case 'd':
			db_path = optarg;
			break;
		case 'o':
			out_path = optarg;
			break;
		default:
			usage(stderr, 1);
		}
	}

	if (argc == optind)
		usage(stderr, 1);
	argc -= optind;
	argv += optind;

	const char *cmd = argv[0];
	if (streq(cmd, "header")) {
		if ((r = db_init(db_path, false, false)) < 0)
			return EXIT_FAILURE;
		if ((r = db_id2header(out_path)) < 0)
			return EXIT_FAILURE;
		db_close();
	} else {
		usage(stderr, 1);
	}

	return EXIT_SUCCESS;
}


static int comp_uint64(const void *e1, const void *e2)
{
	const uint64_t *up1 = e1;
	const uint64_t *up2 = e2;
	const uint64_t u1 = *up1;
	const uint64_t u2 = *up2;

	if (u1 == u2)
		return 0;
	else if (u1 < u2)
		return -1;
	else
		return 1;
}

static int db_step(sqlite3_stmt *stmt)
{
	int r;

	do {
		r = sqlite3_step(stmt);
	} while (r == SQLITE_BUSY);

	return r;
}

SYD_GCC_ATTR((nonnull(1)))
static int db_id2header(const char *path_dst)
{
	int r;
	uint64_t count;
	FILE *dst;

	assert(gdb != NULL);

	dst = fopen(path_dst, "w");
	if (!dst)
		return -errno;
	fprintf(dst, "\
/*\n\
 * sydbox/%s\n\
 *\n\
 * SydBox' Default Network Deny List for modern Linux systems.\n\
 * These are XXH64 hashes of IP addresses gathered from HBlock.\n\
 * Thank you.\n\
 *\n\
 * Copyright (c) 2021 Ali Polatel <alip@exherbo.org>\n\
 * SPDX-License-Identifier: GPL-2.0-only\n\
 */\n\
\n\
#if 0\n\
# *********************************************************\n\
# THIS IS A GENERATED FILE! DO NOT EDIT THIS FILE DIRECTLY!\n\
# *********************************************************\n\
#\n\
#endif\n\n", path_dst);

	/* Step 1: Get number of IDs */
	if (sqlite3_reset(db_stmt[SQL_DB_COUNT_ADDR]) != SQLITE_OK) {
		say("Error during SQLite3 Reset: »%s«", sqlite3_errmsg(gdb));
		return -ECANCELED;
	}

	/* Step and get the count */
	count = 0;
	do {
		r = sqlite3_step(db_stmt[SQL_DB_COUNT_ADDR]);
		if (r == SQLITE_ROW)
			count = (uint64_t)sqlite3_column_int(db_stmt[SQL_DB_COUNT_ADDR],
								   0);
	} while (r == SQLITE_BUSY || r == SQLITE_ROW);

	if (r != SQLITE_DONE) {
		say("Error during SQLite3 Step: »%s«", sqlite3_errmsg(gdb));
		return -ECANCELED;
	}

	/* Reset the statement to its initial state */
	if (sqlite3_reset(db_stmt[SQL_DB_SELECT_ADDR_ID]) != SQLITE_OK) {
		say("Error during SQLite3 Reset: »%s«", sqlite3_errmsg(gdb));
		return -ECANCELED;
	}

	/* Step 2: Allocate the denylist array for QSort */
	if (!count) {
		say("There are no IDs in the »addr« table");
		return -ECANCELED;
	}
	uint64_t *list = malloc(count * sizeof(uint64_t));
	if (!list)
		return -ENOMEM;

	/* Step3: and populate the denylist array. */
	size_t row_count = 0;
	do {
		r = sqlite3_step(db_stmt[SQL_DB_SELECT_ADDR_ID]);
		if (r == SQLITE_ROW) {
			uint64_t id = (uint64_t)sqlite3_column_int(db_stmt[SQL_DB_SELECT_ADDR_ID],
								   0);
			list[row_count++] = id;
		}
	} while (r == SQLITE_BUSY || r == SQLITE_ROW);

	if (r != SQLITE_DONE) {
		say("Error during SQLite3 Step: »%s«", sqlite3_errmsg(gdb));
		return -2;
	}

	/* Step 4: Sort the denylist array */
	clock_t t = clock();
	qsort(list, row_count, sizeof(uint64_t), comp_uint64);
	t = clock() - t;
	double tt = ((double)t)/CLOCKS_PER_SEC;
	say("Sorted »%zu« IP address hashes in »%f« seconds.", row_count, tt);

	/* Step 5: Write the elements of the denylist to the header. */
	for (size_t i = 0; i < row_count; i++) {
		fprintf(dst, "%"PRIu64"UL,\n", list[i]);
	}
	free(list);
	say("Added »%zu« IP address hashes to file »%s«.", row_count, path_dst);

	fclose(dst);
	return 0;
}

static int db_check_version(void)
{
	int r, version = -1;
	bool success;

	assert(gdb != NULL);
	assert(db_stmt_maint[SQL_GET_VERSION] != NULL);
	assert(db_stmt_maint[SQL_SET_VERSION] != NULL);

	/**
	 * Check version
	 */
	do {
		r = sqlite3_step(db_stmt_maint[SQL_GET_VERSION]);
		if (r == SQLITE_ROW)
			version = sqlite3_column_int(db_stmt_maint[SQL_GET_VERSION], 0);
	} while (r == SQLITE_BUSY || r == SQLITE_ROW);

	if (r != SQLITE_DONE) {
		say("Error setting SQLite3 Database Version to »%d«: »%s«",
		    DB_VERSION, sqlite3_errmsg(gdb));
		return -ECANCELED;
	}

	else if (version < DB_MINIMUM_VERSION || version > DB_VERSION) {
		say("Database version mismatch: %d != %d", version, DB_VERSION);
		return -ECANCELED;
	}

	return version == DB_VERSION ? 0 : -EINVAL;
}

bool
db_initialized(void)
{
	return (gdb != NULL);
}


SYD_GCC_ATTR((nonnull(1)))
static int db_init(const char *path, bool create, bool readonly)
{
	int r, flags;
	bool new;

	assert(gdb == NULL);

	new = access(path, F_OK) == 0;

	flags = 0;
	if (create && readonly) {
		say("Invalid flags");
		return -EINVAL;
	}
	flags |= (readonly ? SQLITE_OPEN_READONLY : SQLITE_OPEN_READWRITE);
	if (create)
		flags |= SQLITE_OPEN_CREATE;

	if (sqlite3_open_v2(path, &gdb, flags, NULL) != 0) {
		say("Error opening SQLite3 database »%s«: »%s«",
		    path, sqlite3_errmsg(gdb));
		gdb = NULL;
		return false;
	}

	for (size_t i = 0; i < ELEMENTSOF(db_sql_maint); i++) {
		if (sqlite3_prepare_v2(gdb, db_sql_maint[i], -1,
				&db_stmt_maint[i], NULL) != SQLITE_OK) {
			say("Error preparing SQLite3 statement »%s«: »%s«",
			    db_sql_maint[i], sqlite3_errmsg(gdb));
			db_close();
			return -EINVAL;
		}
	}

	if (new) {
		for (size_t i = 0; i < ELEMENTSOF(db_sql_create); i++) {
			if (sqlite3_prepare_v2(gdb, db_sql_create[i], -1,
					&db_stmt_create[i], NULL) != SQLITE_OK) {
				say("Error preparing SQLite3 statement »%s«: »%s«",
				    db_sql_create[i], sqlite3_errmsg(gdb));
				db_close();
				return -EINVAL;
			}
		}

		if ((r = db_create()) < 0) {
			db_close();
			return r;
		}

		for (size_t i = 0; i < ELEMENTSOF(db_sql_create); i++) {
			if (db_stmt_create[i] != NULL) {
				sqlite3_finalize(db_stmt_create[i]);
				db_stmt_create[i] = NULL;
			}
		}

	}
	else {
		if ((r = db_check_version()) < 0) {
			db_close();
			return r;
		}
	}

	/* Prepare common statements */
	for (size_t i = 0; i < ELEMENTSOF(db_sql); i++) {
		assert(db_stmt[i] == NULL);
		if (sqlite3_prepare_v2(gdb, db_sql[i], -1,
				&db_stmt[i], NULL) != SQLITE_OK) {
			say("Error preparing SQLite3 statement »%s«: »%s«",
			    db_sql[i], sqlite3_errmsg(gdb));
			db_close();
			return -EINVAL;
		}
	}

	return 0;
}

static void db_close(void)
{
	for (size_t i = 0; i < ELEMENTSOF(db_sql_maint); i++) {
		if (db_stmt_maint[i] != NULL) {
			sqlite3_finalize(db_stmt_maint[i]);
			db_stmt_maint[i] = NULL;
		}
	}
	for (size_t i = 0; i < ELEMENTSOF(db_sql); i++) {
		if (db_stmt[i] != NULL) {
			sqlite3_finalize(db_stmt[i]);
			db_stmt[i] = NULL;
		}
	}
	sqlite3_close(gdb);
	gdb = NULL;
}

SYD_GCC_ATTR((nonnull(1)))
static int db_set_authorizer(int (*xAuth)(void *, int, const char *, const char *,
					  const char *,const char *),
			     void *userdata)
{
	assert(gdb != NULL);

	if (sqlite3_set_authorizer(gdb, xAuth, userdata) != SQLITE_OK) {
		say("Error setting SQLite3 Authorizer: »%s«",
		    sqlite3_errmsg(gdb));
		return -ECANCELED;
	}
	return 0;
}

/**
 * Database Maintenance
 */
static int db_create(void)
{
	assert(gdb != NULL);
	assert(db_stmt_create[SQL_DB_CREATE_ADDR] != NULL);
	assert(db_stmt_maint[SQL_SET_ENCODING] != NULL);
	assert(db_stmt_maint[SQL_SET_VERSION] != NULL);

	/**
	 * Create tables
	 */
	if (db_step(db_stmt_create[SQL_DB_CREATE_ADDR]) != SQLITE_DONE) {
		say("Error creating SQLite3 Database »addr«: »%s«",
		    sqlite3_errmsg(gdb));
		return -ENOENT;
	}

	/**
	 * Set encoding
	 */
	if (db_step(db_stmt_maint[SQL_SET_ENCODING]) != SQLITE_DONE) {
		say("Error setting SQLite3 Database Encoding: »%s«",
		    sqlite3_errmsg(gdb));
		return -ECANCELED;
	}

	/**
	 * Set database version
	 */
	if (db_step(db_stmt_maint[SQL_SET_VERSION]) != SQLITE_DONE) {
		say("Error setting SQLite3 Database Version: »%s«",
		    sqlite3_errmsg(gdb));
		return -ECANCELED;
	}

	return 0;
}

/**
 * Execute a single SQL statement without an expected result
 */
static int db_run_stmt(size_t stmt)
{
	assert(gdb != NULL);
	assert(stmt < ELEMENTSOF(db_stmt));

	if (sqlite3_reset(db_stmt_maint[stmt]) != SQLITE_OK) {
		say("Error resetting SQLite3 statement »%s«: »%s«",
		    db_stmt_maint[stmt], sqlite3_errmsg(gdb));
		return -ECANCELED;
	}

	if (db_step(db_stmt_maint[stmt]) != SQLITE_DONE) {
		say("Error stepping statement »%s«: »%s«",
		    db_stmt_maint[stmt], sqlite3_errmsg(gdb));
		return -ECANCELED;
	}

	return 0;
}

/**
 * Database Interaction
 */
static int db_start_transaction(void)
{
	return db_run_stmt(SQL_BEGIN_TRANSACTION);
}

static int db_end_transaction(void)
{
	return db_run_stmt(SQL_END_TRANSACTION);
}

static int db_rollback_transaction(void)
{
	return db_run_stmt(SQL_ROLLBACK_TRANSACTION);
}

static int db_set_sync(bool on)
{
	sqlite3_stmt *stmt;

	assert(gdb != NULL);

	stmt = on ? db_stmt_maint[SQL_PRAGMA_SYNC_ON] :
		db_stmt_maint[SQL_PRAGMA_SYNC_OFF];

	if (sqlite3_reset(stmt) != SQLITE_OK) {
		say("Error resetting SQLite3 Statement »%s«: »%s«",
		    stmt, sqlite3_errmsg(gdb));
		return -ECANCELED;
	}

	if (db_step(stmt) != SQLITE_DONE) {
		say("Error stepping SQLite3 Statement »%s«: »%s«",
		    stmt, sqlite3_errmsg(gdb));
		return -ECANCELED;
	}

	return 0;
}

static int db_vacuum(void)
{
	assert(gdb != NULL);

	if (sqlite3_reset(db_stmt_maint[SQL_VACUUM]) != SQLITE_OK) {
		say("Error resetting SQLite3 Statement »%s«: »%s«",
		    db_stmt_maint[SQL_VACUUM], sqlite3_errmsg(gdb));
		return -ECANCELED;
	}

	if (db_step(db_stmt_maint[SQL_VACUUM]) != SQLITE_DONE) {
		say("Error stepping SQLite3 Statement »%s«: »%s«",
		    db_stmt_maint[SQL_VACUUM], sqlite3_errmsg(gdb));
		return -ECANCELED;
	}

	return 0;
}

static void about(void)
{
	printf(SYD_WARN PACKAGE"-"VERSION GITVERSION SYD_RESET "\n");
}

SYD_GCC_ATTR((noreturn))
static void usage(FILE *outfp, int code)
{
	fprintf(outfp, "\
"PACKAGE"-"VERSION GITVERSION"\n\
Syd's Address Database Maintainance Tool\n\
usage: "PACKAGE" [-hv] {command args...}\n\
-h                          -- Show usage and exit\n\
-v                          -- Show version and exit\n\
Commands:\n\
header [-d db] [-o header]  -- Export database IDs to the given header file\n\
                               Database is »"DB_PATH"« by default.\n\
			       Header is »"HR_PATH"« by default.\n\
\n"SYD_HELPME);
	exit(code);
}

SYD_GCC_ATTR((unused))
static void die(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vsay(stderr, fmt, ap, 'f');
	va_end(ap);
	fputc('\n', stderr);

	exit(EXIT_FAILURE);
}

static void say(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vsay(stderr, fmt, ap, 0);
	va_end(ap);
	fputc('\n', stderr);
}

static void vsay(FILE *fp, const char *fmt, va_list ap, char level)
{
	static int tty = -1;

	if (tty < 0)
		tty = isatty(STDERR_FILENO) == 1 ? 1 : 0;
	if (tty)
		fputs(SYD_WARN, fp);
	if (fmt[0] != ' ')
		fputs(PACKAGE": ", fp);
	switch (level) {
	case 'b':
		fputs("bug: ", fp);
		break;
	case 'f':
		fputs("fatal: ", fp);
		break;
	case 'w':
		fputs("warning: ", fp);
		break;
	default:
		break;
	}
	vfprintf(stderr, fmt, ap);
	if (tty)
		fputs(SYD_RESET, fp);
}
