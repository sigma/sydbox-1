/*
 * sydbox/t/bin/bug-4.c
 *
 * Copyright (c) 2021 Ali Polatel <alip@exherbo.org>
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include "headers.h"

int main(int argc, char *argv[])
{
#ifndef HAVE_NEWFSTATAT
	return ENOSYS;
#else
	int fd;
	const char *trash_directory;
	char *path_name, *path_proc;
	struct stat buf;

	/* Create a temporary file. */
	trash_directory = getenv("TRASH_DIRECTORY");
	if (!trash_directory)
		return EINVAL;
	if (asprintf(&path_name, "%s/deleted-file", trash_directory) < 0)
		return errno;
	unlink(path_name); /* Make "sure" the file does not exist... */
	fd = open(path_name, O_RDONLY|O_CREAT, 0600);
	if (fd < 0)
		return errno;
	unlink(path_name);
	free(path_name);

	/* Now we should have a dangling symbolic link located at:
	 * /proc/fd/${fd} -> ${path_name} (deleted)
	 * and newfstatat() with AT_SYMLINK_NOFOLLOW must succeed.
	 */
	if (asprintf(&path_proc, "/proc/%d/fd/%d", getpid(), fd) < 0)
		return errno;

	/* This fails with ENOENT on sydbox versions affected by #4. */
	errno = 0;
	syscall(SYS_newfstatat, AT_FDCWD, path_proc, &buf, AT_SYMLINK_NOFOLLOW);
	return errno;
#endif
}
