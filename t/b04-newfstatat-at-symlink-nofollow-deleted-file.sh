#!/bin/sh

test_description='bug-4: newfstatat fails with AT_SYMLINK_NOFOLLOW on deleted open file handle under /proc'
. ./test-lib.sh

export TRASH_DIRECTORY
test_expect_success 'newfstatat succeeds with AT_SYMLINK_NOFOLLOW on deleted open file handle under /proc' '
    test_expect_code 0 bug-4
'

test_done
